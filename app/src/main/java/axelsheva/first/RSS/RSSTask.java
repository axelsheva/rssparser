package axelsheva.first.RSS;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.widget.ListView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import axelsheva.first.adapters.ListController;
import axelsheva.first.databases.DatabaseOpenHelper;

/**
 * Created by axelsheva94 on 25.06.2015.
 */
public class RSSTask extends AsyncTask<String, Void, ArrayList<RSSItem>>{
    private ListController listController;
    private ListView listView;
    private ArrayList<RSSItem> items;
    private Context context;
    private DatabaseOpenHelper dbHelper;
    private final SimpleDateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            java.util.Locale.getDefault());

    public void init(ListView listView, ListController listController,
                     ArrayList<RSSItem> items, Context context)
    {
        this.listView = listView;
        this.listController = listController;
        this.items = items;
        this.context = context;
        this.dbHelper = DatabaseOpenHelper.getInstance(context);
    }

    @Override
    protected ArrayList<RSSItem> doInBackground(String... param) {
        SQLiteDatabase sdb = dbHelper.getWritableDatabase();
        try {
            URL url = new URL(param[0]);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            if (httpConn.getResponseCode() == HttpURLConnection.HTTP_OK)
            {
                InputStream inputStream = httpConn.getInputStream();
                System.setProperty("http.agent", "");
                parse(inputStream, sdb);
                inputStream.close();
            }
            httpConn.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fill(sdb);
        sdb.close();
        Collections.sort(items, Collections.reverseOrder());
        return items;
    }

    private void parse(InputStream inputStream, SQLiteDatabase sdb)
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db;
        Document document = null;
        try {
            db = dbf.newDocumentBuilder();
            document = db.parse(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(document != null)
        {
            Element element = document.getDocumentElement();
            NodeList nodeList = element.getElementsByTagName("item");
            DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz",
                    java.util.Locale.getDefault());
            if (nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); ++i) {
                    Element entry = (Element) nodeList.item(i);
                    Element eLink = (Element) entry.getElementsByTagName("link").item(0);
                    String sLink = eLink.getFirstChild().getNodeValue();
                    if(existsNews(sdb, sLink) == 0)
                    {
                        Element eTitle = (Element) entry.getElementsByTagName("title").item(0);
                        String sTitle = eTitle.getFirstChild().getNodeValue();
                        Element ePubDate = (Element) entry.getElementsByTagName("pubDate").item(0);
                        Date dPubDate;
                        try {
                            dPubDate = formatter.parse(ePubDate.getFirstChild().getNodeValue());
                        } catch(ParseException pe) {
                            throw new IllegalArgumentException();
                        }
                        long id = insertNews(sdb, sTitle, sLink, dPubDate);
                        String html = getHTML(sLink);
                        writeToFile(html, id + ".html");
                    }
                }
            }
        }
    }

    private int existsNews(SQLiteDatabase sdb, String link)
    {
        int count;
        Cursor cursor = sdb.rawQuery("select " + DatabaseOpenHelper.ID_COLUMN +
                                    " from " + DatabaseOpenHelper.NEWS_TABLE +
                                    " where " + DatabaseOpenHelper.LINK_COLUMN +
                                    " = \"" + link + "\"", null);
        count = cursor.getCount();
        cursor.close();
        return count;
    }

    private long insertNews(SQLiteDatabase sdb, String title, String link, Date pubDate)
    {
        String sDate = iso8601Format.format(pubDate);
        ContentValues cv = new ContentValues();
        cv.put(DatabaseOpenHelper.TITLE_COLUMN, title);
        cv.put(DatabaseOpenHelper.LINK_COLUMN, link);
        cv.put(DatabaseOpenHelper.PUBDATE_COLUMN, sDate);
        return sdb.insert(DatabaseOpenHelper.NEWS_TABLE, null, cv);
    }

    private String getHTML(String link) {
        try {
            URL url = new URL(link);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = httpURLConnection.getInputStream();

                String html = convertStreamToString(inputStream);
                html = html.substring(html.indexOf("<div class=\"content html_format\">"),
                        html.indexOf("<ul class=\"tags\">"));
                html = "<meta http-equiv=\"content-type\" content=\"text/html; charset=utf-8\" />"
                        + html;
                return html;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void fill(SQLiteDatabase sdb)
    {
        Cursor cursor = sdb.query(DatabaseOpenHelper.NEWS_TABLE, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int idColIndex = cursor.getColumnIndex(DatabaseOpenHelper.ID_COLUMN);
            int titleColIndex = cursor.getColumnIndex(DatabaseOpenHelper.TITLE_COLUMN);
            int linkColIndex = cursor.getColumnIndex(DatabaseOpenHelper.LINK_COLUMN);
            int pubDateColIndex = cursor.getColumnIndex(DatabaseOpenHelper.PUBDATE_COLUMN);
            do {
                try {
                    items.add(new RSSItem(cursor.getString(titleColIndex),
                            iso8601Format.parse(cursor.getString(pubDateColIndex)),
                            cursor.getString(linkColIndex), cursor.getInt(idColIndex)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private void writeToFile(String data, String name) {
        try {
            OutputStreamWriter outputStreamWriter = new
                    OutputStreamWriter(context.openFileOutput(name, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPostExecute(ArrayList<RSSItem> result) {
        super.onPostExecute(result);
        listController.start(listView, result);
    }
}
