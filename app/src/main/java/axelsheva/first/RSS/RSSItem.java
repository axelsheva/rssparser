package axelsheva.first.RSS;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by axelsheva94 on 23.06.2015.
 */
public class RSSItem implements Comparable<RSSItem> {
    private String title;
    private Date pubDate;
    private String link;
    private long id;

    public RSSItem(String title, Date pubDate, String link, long id) {
        this.title = title;
        this.pubDate = pubDate;
        this.link = link;
        this.id = id;
    }

    public RSSItem(RSSItem source) {
        this.title = source.title;
        this.pubDate = source.pubDate;
        this.link = source.link;
        this.id = source.id;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getLink()
    {
        return this.link;
    }

    public Date getPubDate()
    {
        return this.pubDate;
    }

    public long getId()
    {
        return this.id;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy, HH:mm",
                java.util.Locale.getDefault());
        return getTitle() + " (" + sdf.format(this.getPubDate()) + ")";
    }

    @Override
    public int compareTo(RSSItem o) {
        return getPubDate().compareTo(o.getPubDate());
    }
}
