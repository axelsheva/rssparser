package axelsheva.first.databases;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by axelsheva94 on 22.06.2015.
 */
public class DatabaseOpenHelper extends SQLiteOpenHelper
{
    private static DatabaseOpenHelper sInstance;
    private static final String DB_NAME = "sql.db";

    public static final String ID_COLUMN = "id";
    public static final String TITLE_COLUMN = "title";
    public static final String LINK_COLUMN = "link";
    public static final String PUBDATE_COLUMN = "pubDate";
    public static final String ID_NEWS_COLUMN = "id_news";
    public static final String NEWS_TABLE = "news";
    public static final String FAVORITES_TABLE = "menu_news";

    public static DatabaseOpenHelper getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseOpenHelper(context.getApplicationContext());
        }
        return sInstance;
    }

    private DatabaseOpenHelper(Context context)
    {
        super(context, DB_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("create table " + NEWS_TABLE + " (" + ID_COLUMN +
                " integer primary key autoincrement, " + TITLE_COLUMN +
                " text, " + PUBDATE_COLUMN + " text, " + LINK_COLUMN + " text);");
        db.execSQL("create table " + FAVORITES_TABLE +
                " (" + ID_COLUMN + " integer primary key autoincrement, " +
                ID_NEWS_COLUMN + " integer);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
}
