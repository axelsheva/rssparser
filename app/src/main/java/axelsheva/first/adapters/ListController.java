package axelsheva.first.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import axelsheva.first.RSS.RSSItem;

/**
 * Created by axelsheva94 on 25.06.2015.
 */
public class ListController {
    private Context context;

    public ListController(Context context)
    {
        this.context = context;
    }

    public void start(ListView listView, ArrayList<RSSItem> items)
    {
        ArrayAdapter<RSSItem> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, items);
        listView.setAdapter(arrayAdapter);
    }
}
