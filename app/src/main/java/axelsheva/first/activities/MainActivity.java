package axelsheva.first.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import axelsheva.first.databases.DatabaseOpenHelper;
import axelsheva.first.adapters.ListController;
import axelsheva.first.R;
import axelsheva.first.RSS.RSSItem;
import axelsheva.first.RSS.RSSTask;

public class MainActivity extends ActionBarActivity {
    private DatabaseOpenHelper dbHelper;
    private ListView listView;
    private ListController listController;
    private ArrayList<RSSItem> items;
    private ArrayList<RSSItem> found;
    private EditText editText;
    private final SimpleDateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
            java.util.Locale.getDefault());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        listView = (ListView)findViewById(R.id.listView);
        listController = new ListController(this);
        items = new ArrayList<>();
        dbHelper = DatabaseOpenHelper.getInstance(this);
        editText = (EditText)findViewById(R.id.editText);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, FullNewsActivity.class);
                RSSItem item = new RSSItem((RSSItem)parent.getItemAtPosition(position));
                intent.putExtra("id", item.getId());
                intent.putExtra("title", item.getTitle());
                intent.putExtra("link", item.getLink());
                SQLiteDatabase sdb = dbHelper.getWritableDatabase();
                Cursor cursor = sdb.rawQuery("select " + DatabaseOpenHelper.ID_COLUMN +
                        " from " + DatabaseOpenHelper.FAVORITES_TABLE +
                        " where " + DatabaseOpenHelper.ID_NEWS_COLUMN + " = " +
                        String.valueOf(item.getId()), null);
                if(cursor.getCount() == 0)
                    intent.putExtra("favorites", 0);
                else
                    intent.putExtra("favorites", 1);
                cursor.close();
                startActivity(intent);
            }
        });

        RSSTask rssTask = new RSSTask();
        rssTask.init(listView, listController, items, this);
        rssTask.execute("http://habrahabr.ru/rss/feed/posts/baddc0c4d1ae69b10b009291542c97b5/");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_favorites:
                showFavorites();
                return true;
            case R.id.reset_search:
                listController.start(listView, items);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void search(View v)
    {
        if(!editText.getText().toString().equals(""))
        {
            found = new ArrayList<>();
            for (Object o : items)
            {
                if (o.toString().toLowerCase().contains(editText.getText().toString().toLowerCase()))
                {
                    found.add((RSSItem)o);
                }
            }
            listController.start(listView, found);
            editText.setText("");
        }
        else
            listController.start(listView, items);
    }

    private void showFavorites()
    {
        SQLiteDatabase sdb = dbHelper.getWritableDatabase();
        Cursor cursor = sdb.rawQuery(
                "select " +
                    DatabaseOpenHelper.NEWS_TABLE + "." + DatabaseOpenHelper.ID_COLUMN + ", " +
                    DatabaseOpenHelper.NEWS_TABLE + "." + DatabaseOpenHelper.TITLE_COLUMN + ", " +
                    DatabaseOpenHelper.NEWS_TABLE + "." + DatabaseOpenHelper.LINK_COLUMN + ", " +
                    DatabaseOpenHelper.NEWS_TABLE + "." + DatabaseOpenHelper.PUBDATE_COLUMN +
                " from " + DatabaseOpenHelper.NEWS_TABLE +
                " join " + DatabaseOpenHelper.FAVORITES_TABLE +
                " on " + DatabaseOpenHelper.NEWS_TABLE +
                "." + DatabaseOpenHelper.ID_COLUMN +
                "=" + DatabaseOpenHelper.ID_NEWS_COLUMN +
                " where " + DatabaseOpenHelper.ID_NEWS_COLUMN + ">=1", null);
        found = new ArrayList<>();
        if (cursor.moveToFirst()) {
            int idColIndex = cursor.getColumnIndex(DatabaseOpenHelper.ID_COLUMN);
            int titleColIndex = cursor.getColumnIndex(DatabaseOpenHelper.TITLE_COLUMN);
            int linkColIndex = cursor.getColumnIndex(DatabaseOpenHelper.LINK_COLUMN);
            int pubDateColIndex = cursor.getColumnIndex(DatabaseOpenHelper.PUBDATE_COLUMN);
            do {
                try {
                    found.add(new RSSItem(cursor.getString(titleColIndex),
                            iso8601Format.parse(cursor.getString(pubDateColIndex)),
                            cursor.getString(linkColIndex), cursor.getInt(idColIndex)));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } while (cursor.moveToNext());
        }
        cursor.close();
        sdb.close();
        listController.start(listView, found);
    }
}
