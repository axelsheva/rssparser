package axelsheva.first.activities;

import android.content.ContentValues;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import axelsheva.first.databases.DatabaseOpenHelper;
import axelsheva.first.R;

/**
 * Created by axelsheva94 on 25.06.2015.
 */
public class FullNewsActivity extends ActionBarActivity {
    private DatabaseOpenHelper dbHelper;
    private boolean favorites = false;
    private int id;
    private String link;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.full_news);

        dbHelper = DatabaseOpenHelper.getInstance(this);
        id = Integer.parseInt(getIntent().getExtras().get("id").toString());
        WebView content = (WebView)findViewById(R.id.content);
        content.loadUrl("file:///data/data/axelsheva.first/files/" + id + ".html");
        setTitle(getIntent().getStringExtra("title"));
        link = getIntent().getStringExtra("link");
        if(Integer.parseInt(getIntent().getExtras().get("favorites").toString()) == 1)
            favorites = true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_news, menu);
        if(!favorites)
            menu.getItem(0).setIcon(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
        else
            menu.getItem(0).setIcon(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.favorites:
                if(!favorites)
                {
                    if(setFavorite() != -1)
                    {
                        favorites = true;
                        item.setIcon(R.drawable.abc_btn_rating_star_on_mtrl_alpha);
                    }
                }
                else
                {
                    if(delFavorite() != 0)
                    {
                        favorites = false;
                        item.setIcon(R.drawable.abc_btn_rating_star_off_mtrl_alpha);
                    }
                }
                return true;
            case R.id.url:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
                startActivity(browserIntent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private long setFavorite()
    {
        SQLiteDatabase sdb = dbHelper.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(DatabaseOpenHelper.ID_NEWS_COLUMN, id);
        long result = sdb.insert(DatabaseOpenHelper.FAVORITES_TABLE, null, cv);
        sdb.close();
        return result;
    }

    private long delFavorite()
    {
        SQLiteDatabase sdb = dbHelper.getWritableDatabase();
        long result = sdb.delete(DatabaseOpenHelper.FAVORITES_TABLE,
                DatabaseOpenHelper.ID_NEWS_COLUMN + "=" + id, null);
        sdb.close();
        return result;
    }
}
